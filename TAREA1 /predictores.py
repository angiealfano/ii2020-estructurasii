import sys
from collections import deque
import os
import gzip

#Primero se ingresan los parametros por medio de la consola
s = int(sys.argv[2])
bp = int(sys.argv[4])
gh = int(sys.argv[6])
ph = int(sys.argv[8])

#Predictores
#Esta funcion es la encargada de imprimir los resultados
def print_information(s,bp,gh,ph, correct_taken, correct_notaken, incorrect_notaken, incorrect_taken, num_branches):
    print ("------------------------------------------------------------------------")
    print ("\nPrediction parameters:")
    print ("\n------------------------------------------------------------------------")
    if (bp == 0):
        print ("\nBranch prediction type: Bimodal")
    elif (bp == 2):
        print ("\nBranch prediction type: GShare")
    elif (bp == 1):
        print ("\nBranch prediction type: PShare")
    elif (bp == 3):
        print ("\nBranch prediction type: Tournament")
    print ("\nBHT size (entries): " + str(pow(2,s)))
    print ("\nGlobal history register size: " + str(gh))
    print ("\nPrivate history register size: " + str(ph))
    print ("\n------------------------------------------------------------------------")
    print ("\n------------------------------------------------------------------------")
    print ("\nSimulation results:")
    print ("\n------------------------------------------------------------------------")
    print ("\nNumber of branches: " + str(num_branches))
    print ("\nNumber of correct predictions of taken branches: " + str(correct_taken))
    print ("\nNumber of incorrect predictions of taken branches: " + str(incorrect_taken))
    print ("\nNumber of correct predictions of not-taken branches: " + str(correct_notaken))
    print ("\nNumber of incorrect predictions of not-taken branches: " + str(incorrect_notaken))
    Percentage_correct = (100*(correct_taken + correct_notaken))/num_branches
    print ("\nPercentage of correct predictions: {:.2f}".format(round(Percentage_correct,2)))
    print ("------------------------------------------------------------------------")


#Esta es la funcion para leer el archivo y procesarlo, se dividen por un lados los resultados del PC y en otra lista el valor de la salida.
#Para la implementacion de los predictores se necesita el valor de los s bits de PC en binario y en decimal. 
#Como son s cantidad de bits la  variable se le asigna el nombre de pc_s, esta sera utilizada en otras funciones.
def get_index(s):
    pcs_binary = []
    pc_s_array = []
    results = []
    gzip_traces = sys.stdin.read()
    traces = gzip_traces.split("\n")
    
    for i in range (len(traces) -1):
        line = traces[i]
        pc_and_result = line.split(" ")
        pc = pc_and_result[0]
        result = pc_and_result[1]
        results.append(result)

        pc_bin = bin(int(pc))

        pcs_binary.append(pc_bin)

        pc_s = pc_bin[-s:]
    

        pc_s = int(pc_s, 2)

        pc_s_array.append (pc_s)


    return pcs_binary, pc_s_array, results


#Esta es la funcion que se encarga de revisar el contador y predecir.
# La asignacion de valores es la siguiente:
# 00 = Strong Not Taken
# 01 = Weak Not Taken
# 10 = Weak  Taken
# 11 = Strong Taken
def predictor_2bits(BHT, last_pc):
    last_counter = BHT[last_pc]
    if last_counter == '00':   
        return 'N'
    elif last_counter == '01':
        return 'N'
    elif last_counter == '10':       
        return 'T'
    elif last_counter == '11':       
        return 'T'

# La siguiente funcion se encarga de actualizar los contadores, dependiendo de lo que hubo en la ultima salida
def upgrade_counter(result, BHT, last_pc):
    last_counter = BHT[last_pc]
    if result == 'T':
        if  last_counter == '00':
            BHT[last_pc] = '01'
        elif last_counter == '01':
            BHT[last_pc] = '10'
        elif last_counter == '10':
            BHT[last_pc] = '11'
        elif last_counter == '11':
            BHT[last_pc] = '11'
    else :
        if last_counter == '00':
            BHT[last_pc] = '00'
        elif last_counter == '01':
            BHT[last_pc] = '00'
        elif last_counter == '10':
            BHT[last_pc] = '01'
        elif last_counter == '11':
            BHT[last_pc] = '10'


#Este es el predictor bimodal, donde se utilizan las funciones anteriores.
#Ademas se evalua el valor de la prediccion para poder darle los valores de si fue tomada o no la rama, de manera correcta o incorrecta.
def bimodal_branch_predictor(s):
    entries = pow(2,s)
    pc_s = []
    result = []
    #Se empiezan los contadores en strong not taken
    BHT= ['00' for i in range (entries)] 
    correct_taken = 0
    incorrect_taken = 0
    correct_notaken = 0
    incorrect_notaken = 0
    index1 = get_index(s)
    pc_s = index1[1]
    result = index1[2]
    num_branches=len(pc_s)
    for i in range(len(pc_s)):
        last_pc = pc_s[i]
        last_result = result[i]
        prediction = predictor_2bits(BHT,last_pc)
        upgrade_counter(last_result,BHT,last_pc)
    
        if prediction == "T":
            if prediction == last_result:
                correct_taken += 1
            else:
                incorrect_notaken += 1
        else:
            if prediction == last_result:
                correct_notaken += 1
            else:
                incorrect_taken += 1

    print_information(s,bp,gh,ph, correct_taken, correct_notaken, incorrect_notaken, incorrect_taken, num_branches)


#Funcion que sirve para desplazar el registro global hacia la izquierda y agregar el ultimo bit
def global_register_append(global_register, gh, last_result):
    if len(global_register) < gh:
        global_register.append(last_result)    
    else:
        global_register.popleft() 
        global_register.append(last_result)   

#Esta es la funcion donde se realiza el xor, que se necesita para el Gshare y el PShare
#El valor de la XOR se utiliza para indexar por lo que se necesita en decimal
def xor(register_i,last_pc,lastpcs_bin):
    if len(register_i) == 0: 
        return last_pc
    else:
        xor = int(register_i,2)^int(lastpcs_bin,2)
        xor_binary = bin(int(xor))
        xor_lsb = xor_binary[-s:]
        xor_value = int(xor_lsb,2)
        return xor_value 

#Este es el predictor global, que utiliza las funciones anteriores.
#Hay una nueva variable que es el registro global, donde se uso un deque, para poder ingresar y sacar valores por ambos lados.
#Al ser un deque, necesitamos ese valor como string para poder trabajarlo por lo que se encontrara en el codigo la conversion.
def globalhistory_branch_predictor(gh,s):
    globalhistory_register_deque = deque()
    entries = pow(2,s)
    pc_s = []
    result = []
    #Se empiezan los contadores en strong not taken
    BHT= ['00' for i in range (entries)] 
    correct_taken = 0
    incorrect_taken = 0
    correct_notaken = 0
    incorrect_notaken = 0
    index1 = get_index(s)
    pcs_bin = index1[0]
    pc_s = index1[1]
    result = index1[2]
    num_branches=len(pc_s)
    for i in range(num_branches):
        last_pc = pc_s[i]
        last_result = result[i]
        lastpcs_bin = pcs_bin [i] 
        globalhistory_register_str = ""
        for register in globalhistory_register_deque: 
            globalhistory_register_str = globalhistory_register_str + str(register)
        xor_value = xor(globalhistory_register_str,last_pc,lastpcs_bin)
        prediction = predictor_2bits(BHT,xor_value)
        upgrade_counter(last_result,BHT,xor_value)
  
        if prediction=="T":
            if prediction == last_result:
                global_register_append(globalhistory_register_deque, gh, 1)
                correct_taken += 1
            else:
                global_register_append(globalhistory_register_deque, gh, 0)
                incorrect_notaken += 1
        else:   
            if prediction == last_result:
                global_register_append(globalhistory_register_deque, gh, 0)
                correct_notaken += 1
            else:
                global_register_append(globalhistory_register_deque, gh, 1)
                incorrect_taken += 1
        
    print_information(s,bp,gh,ph, correct_taken, correct_notaken, incorrect_notaken, incorrect_taken, num_branches)


#Funcion que sirve para desplazar el registro hacia la izquierda y agregar el ultimo bit
def private_register_append(index, PHT, ph, last_result):
    if len(PHT[index]) < ph:
        PHT[index].append(last_result)    
    else:
        PHT[index].popleft() 
        PHT[index].append(last_result)  

#Este es el predictor privado, donde ahora se tendra una nueva tabla PHT, que en esta se encontraran los bits de historia.
#Los bits de historia tambien se iran guardando como un deque, al igual que en el predictor anterior
def privatehistory_branch_predictor(ph,s):
    entries = pow(2,s)
    pc_s = []
    result = []
    #Se empiezan los contadores en strong not taken
    BHT= ['00' for i in range (entries)] 
    PHT = [deque() for i in range (entries)] 
    correct_taken = 0
    incorrect_taken = 0
    correct_notaken = 0
    incorrect_notaken = 0
    index1 = get_index(s)
    pcs_bin = index1[0]
    pc_s = index1[1]
    result = index1[2]
    num_branches=len(pc_s)
    for i in range(num_branches):
        last_pc = pc_s[i]
        last_result = result[i]
        lastpcs_bin = pcs_bin [i] 
        privatehistory_register_deque = PHT [last_pc]
        privatehistory_register_str = ""
        for register in privatehistory_register_deque: 
            privatehistory_register_str = privatehistory_register_str + str(register)
        xor_value = xor(privatehistory_register_str,last_pc,lastpcs_bin)
        prediction = predictor_2bits(BHT,xor_value)
        upgrade_counter(last_result,BHT,xor_value)

        if prediction == "T":
            if prediction == last_result:
                private_register_append(last_pc, PHT, ph, 1)
                correct_taken += 1
            else:
                private_register_append(last_pc, PHT, ph, 0)
                incorrect_notaken += 1
        else:
            if prediction == last_result:
                private_register_append(last_pc, PHT, ph, 0)
                correct_notaken += 1
            else:
                private_register_append(last_pc, PHT, ph, 1)
                incorrect_taken += 1
        
    print_information(s,bp,gh,ph, correct_taken, correct_notaken, incorrect_notaken, incorrect_taken,num_branches)

def tournament_prediction(MTP, last_pc):
    last_counter = MTP[last_pc]
    if last_counter == '00':   
        return 'P'
    elif last_counter == '01':
        return 'P'
    elif last_counter == '10':       
        return 'G'
    elif last_counter == '11':       
        return 'G'

def upgrade_MTP(last_result,pshare_prediction,gshare_prediction, MTP, last_pc):
    last_counter = MTP[last_pc]
    if gshare_prediction == last_result:
        if last_counter == '00':
            MTP[last_pc] = '01'
        elif last_counter == '01':
            MTP[last_pc] = '10'
        elif last_counter == '10':
            MTP[last_pc] = '11'
        elif last_counter == '11':
            MTP[last_pc] = '11'
    elif pshare_prediction == last_result:
        if last_counter == '00':
            MTP[last_pc] = '00'
        elif last_counter == '01':
            MTP[last_pc] = '00'
        elif last_counter == '10':
            MTP[last_pc] = '01'
        elif last_counter == '11':
            MTP[last_pc] = '10'



#Esta es la funcion principal del predictor por torneo.
#La asignacion es la siguiente:
#00 = strongly prefer pshared
#01 = weakly prefer pshared
#10 = weakly prefer gshared
#11 = strongly prefer gshared
def tournament_branch_predictor(s,gh,ph):
    entries = pow(2,s)
    globalhistory_register_deque = deque()
    MTP=['00' for i in range (entries)]
    entries = pow(2,s)
    pc_s = []
    result = []
    #Se empiezan los contadores en strong not taken
    BHT_P= ['00' for i in range (entries)] 
    BHT_G= ['00' for i in range (entries)] 
    PHT = [deque() for i in range (entries)] 
    correct_taken = 0
    incorrect_taken = 0
    correct_notaken = 0
    incorrect_notaken = 0
    index1 = get_index(s)
    pcs_bin = index1[0]
    pc_s = index1[1]
    result = index1[2]
    num_branches=len(pc_s)
    for i in range(num_branches):
        last_pc = pc_s[i]
        last_result = result[i]
        lastpcs_bin = pcs_bin [i] 
        privatehistory_register_deque = PHT [last_pc]
        privatehistory_register_str = ""
        for register in privatehistory_register_deque: 
            privatehistory_register_str = privatehistory_register_str + str(register)
        xor_value_h = xor(privatehistory_register_str,last_pc,lastpcs_bin)
        pshare_prediction = predictor_2bits(BHT_P,xor_value_h)
        upgrade_counter(last_result,BHT_P,xor_value_h)
        globalhistory_register_str = ""
        for register in globalhistory_register_deque: 
            globalhistory_register_str = globalhistory_register_str + str(register)
        xor_value_g = xor(globalhistory_register_str,last_pc,lastpcs_bin)
        gshare_prediction = predictor_2bits(BHT_G,xor_value_g)
        upgrade_counter(last_result,BHT_G,xor_value_g)

        
        if last_result=="T":
            global_register_append(globalhistory_register_deque, gh, 1)
            private_register_append(last_pc, PHT, ph, 1)

        else:
            global_register_append(globalhistory_register_deque, gh, 0)
            private_register_append(last_pc, PHT, ph, 0)


        MTP_prediction=tournament_prediction(MTP,last_pc)
        if MTP_prediction=='P':
            prediction=pshare_prediction
            if prediction == "T":
                if prediction == last_result:
                    correct_taken += 1
                else:
                    incorrect_notaken += 1
            else:
                if prediction == last_result:
                    correct_notaken += 1
                else:
                    incorrect_taken += 1

        else:
            prediction=gshare_prediction
            if prediction == "T":
                if prediction == last_result:
                    correct_taken += 1
                else:
                    incorrect_notaken += 1
            else:
                if prediction == last_result:
                    correct_notaken += 1
                else:
                    incorrect_taken += 1



        if (pshare_prediction!=gshare_prediction):
           
            upgrade_MTP(last_result,pshare_prediction,gshare_prediction,MTP,last_pc)
        else: 
                continue
        
            
    print_information(s,bp,gh,ph, correct_taken, correct_notaken, incorrect_notaken, incorrect_taken,num_branches)






def main():
    if (bp==0):
        bimodal_branch_predictor(s)
    elif (bp==2):
        globalhistory_branch_predictor(gh,s)
    elif (bp==1):
        privatehistory_branch_predictor(ph,s)
    elif (bp==3):
        tournament_branch_predictor(s,gh,ph)
main()
