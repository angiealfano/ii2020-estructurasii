# Proyecto Predictores de Salto

Programa en Python (Python 3) que realiza las predicciones de salto de un archivo que posee los PCs y las salidas.
Posee 4 tipos de predictores:
* Bimodal
* PShare
* Gshare
* Tournament

Al correr el programa se obtienen los resultados dependiendo de los parametros ingresados.
En el git se presenta un pipeline el cual los revisa automaticamente, mediante la ejecucion de un script de bash.

## Requerimientos
* Python 3 en adelante


## Uso
```bash
gunzip -c branch-trace-gcc.trace.gz | python3 predictores.py  -s 5  -bp 3 -gh 5 -ph 3 
```


